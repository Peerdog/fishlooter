import org.rspeer.runetek.adapter.scene.Pickable;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.StopWatch;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.scene.Pickables;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.script.Script;
import org.rspeer.script.ScriptCategory;
import org.rspeer.script.ScriptMeta;
import org.rspeer.ui.Log;

import java.awt.*;

@ScriptMeta(name = "Peerdog's FishLooter",  desc = "Takes at Trout & Salmon at Barbarian Village and banks them.", developer = "Peerdog", category = ScriptCategory.MONEY_MAKING)


public class Main extends Script implements RenderListener {

    Area RIVER_AREA =  Area.rectangular(3100, 3436, 3110, 3422);
    Area BANK_AREA =  Area.rectangular(3094, 3493, 3092, 3489);
    Player local = Players.getLocal();
    private int sleepTime;
    StopWatch time;
    public long totalFish = 0;


    @Override
    public void onStart() {
        Log.info("Starting...");
        time = StopWatch.start();
    }
    public void onStop() { Log.info ("Stopping..."); }

    private void bank() {
        if (Bank.isOpen()) {
            long tempFish = Inventory.getCount("Raw Salmon", "Raw Trout");
            Bank.depositInventory();
            Time.sleepUntil(Inventory::isEmpty, 2000);
            if (Inventory.isEmpty()) {
                totalFish += tempFish;
                Bank.close();
            } }
        else {
            final SceneObject Bank_Booth = SceneObjects.getNearest("Bank booth");
            if (Bank_Booth != null) {
                Bank_Booth.interact(a -> true);
                Time.sleep(500, 700);
            }
        }
    }


    @Override
    public int loop() {
        if (Movement.isRunEnabled()) {
            sleepTime = 2000;
        }else{
            sleepTime = 4000;
        }

        if (!Movement.isRunEnabled() && Movement.getRunEnergy() > 30 && !Bank.isOpen()) {
            Movement.toggleRun(true);
            return 1000;
        }

        if (!Inventory.isFull()) {
            if (RIVER_AREA.contains(local)) {
                if (local.getAnimation() == -1 && !local.isMoving()) {
                    Log.info("Looking for fish...");
                    Pickable fish = Pickables.getNearest("Raw Salmon", "Raw Trout");
                    if (fish != null) {
                        if (Movement.isWalkable(fish)) {
                            fish.interact("Take");
                            return 400;
                        }

                    }
                }
            }else{
                Log.info("Walking to river...");
                Time.sleep(sleepTime);
                Movement.walkTo(RIVER_AREA.getCenter());


            }

        } else {
            if (Inventory.isFull()) {
                if (BANK_AREA.contains(local)) {
                    bank();
                }else{
                    Log.info("Walking to bank...");
                    Movement.walkTo(BANK_AREA.getCenter());
                    Time.sleep(sleepTime);
                }
            }else{

            }
        }
            return 1000;
    }

    @Override
    public void notify(RenderEvent e) {
        long seconds = time.getElapsed().getSeconds();
        long fish = totalFish;
        long fishPerHour = 0;
        if(seconds>0){
            fishPerHour =  fish * 3600 / seconds;
        }

        Graphics g = e.getSource();
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setFont(new Font("default", Font.BOLD, 16));
        int y = 35;
        int x = 10;
        g2.setColor(Color.cyan);
        g2.drawString("Peerdog's FishLooter", x, y);
        g2.drawString("Runtime: " + time.toElapsedString(), x, y += 20);
        g2.drawString("Total Fish: " + fish, x, y += 20);
        g2.drawString("Fish per hour: " + fishPerHour, x, y += 20);
    }

}
